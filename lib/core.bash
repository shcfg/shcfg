#!/bin/bash
# Copyright 2020 Andanan
#
# This file is part of shcfg, a configuration management framework
# for the bash shell. For more information take a look at
# <https://gitlab.com/shcfg/shcfg>.
#
# shcfg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# shcfg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# (de)activate module
_shcfg__activate_module() {
	local module="$1"
	# add to active module index
	_shcfg__add_module_to_active_index "${module}" || return $?
	# add to config file
	_shcfg__add_module_to_cfg "${module}"
}
_shcfg__deactivate_module() {
	local module="$1"
	# remove from index array
	_shcfg__remove_module_from_active_index "${module}" || return $?
	# remove from config file
	_shcfg__remove_module_from_cfg "${module}"
}

# (un)install module
_shcfg__uninstall_module() {
	local module="$1" module_dir repo_modules_dir
	module_dir="${_SHCFG_MODULES_DIR}/${module}"
	repo_modules_dir="${_SHCFG_MODULES_DIR}/${module%%/*}"
	if [ ! -d "${module_dir}" ]; then
		# module is not installed
		[ "${SHCFG_DEBUG}" = 'true' ] && echo "[DEBUG] module already uninstalled: ${module}" >&2
		return
	fi
	[ "${SHCFG_DEBUG}" = 'true' ] && echo "[DEBUG] Uninstalling module: ${module}" >&2
	command rm -r "${module_dir}"
	if [ "$(find "${repo_modules_dir}" -mindepth 1 | wc -l)" -eq 0 ]; then
		[ "${SHCFG_DEBUG}" = 'true' ] && echo "[DEBUG] Removing dir: ${repo_modules_dir}" >&2
		command rm -r "${repo_modules_dir}"
	fi
	if _shcfg__is_module_active "${module}"; then
		_shcfg__deactivate_module "${module}"
	fi
	[ ! -d "${module_dir}" -a "${SHCFG_DEBUG}" = 'true' ] && echo "[DEBUG] Uninstalled module: ${module}"
}
_shcfg__install_module() {
	local module="$1" module_dir ref
	module_dir="${_SHCFG_MODULES_DIR}/${module}"
	_shcfg__get_module_ref "${module}" || return $?
	ref="${_result}"
	_result=
	_shcfg__ensure_component_installed "${module_dir}" "${ref}" "module" "${module}" || return $?
	_shcfg__activate_module "${module}"
}
_shcfg__cleanup_modules() {
	local repo_dir module
	while read -r line || [[ -n ${line} ]]; do
		repo_dir="$(dirname "$line")"
		module="${repo_dir##*/}/${line##*/}"
		# TODO: remove check for activation as soon as the module-activation subsystem is implemented
		if ! _shcfg__is_module_active "${module}" || ! _shcfg__is_module_indexed "${module}"; then
			_shcfg__uninstall_module "${module}"
		fi
	done < <(find "${_SHCFG_MODULES_DIR}" -mindepth 2 -maxdepth 2 -type d )
}
_shcfg__is_module_installed() {
	local module_dir
	module_dir="${_SHCFG_MODULES_DIR}/$1"
	[ -d "${module_dir}" ]
}

# (un)install repo
_shcfg__uninstall_module_repo() {
	local repo modules_dir repo_dir
	repo="$1"
	modules_dir="${_SHCFG_MODULES_DIR}/${repo}"
	repo_dir="${_SHCFG_REPOS_DIR}/${repo}"
	if [ -d "${modules_dir}" ]; then
		# remove installed modules
		command rm -r "${modules_dir}"
	fi
	if [ ! -d "${repo_dir}" ]; then
		# repo is not installed
		[ "${SHCFG_DEBUG}" = 'true' ] && echo "[DEBUG] module repo already uninstalled: ${repo}" >&2
		return
	fi
	[ "${SHCFG_DEBUG}" = 'true' ] && echo "[DEBUG] Uninstalling module repo: ${repo}" >&2
	command rm -r "${repo_dir}"
}
_shcfg__install_module_repo() {
	local repo repo_dir ref
	repo="$1"
	repo_dir="${_SHCFG_REPOS_DIR}/${repo}"
	_shcfg__get_repo_ref "${repo}" || return $?
	ref="${_result}"
	_result=
	_shcfg__ensure_component_installed "${repo_dir}" "${ref}" "repo" "${repo}"
}
_shcfg__cleanup_repos() {
	local repo=
	while read -r line || [[ -n ${line} ]]; do
		repo="${line##*/}"
		if ! _shcfg__is_repo_indexed "${repo}"; then
			_shcfg__uninstall_module_repo "${repo}"
		fi
	done < <(find "${_SHCFG_REPOS_DIR}" -mindepth 1 -maxdepth 1 -type d )
}

# install helpers
_shcfg__ensure_component_installed() {
	local target_dir ref component_type component_name remote_ref
	target_dir="$1"
	ref="$2"
	component_type="${3:-component}"
	component_name="${4:-_unknown_}"
	if [ -d "${target_dir}" ]; then
		remote_ref="$(_shcfg__print_git_remote_url "${target_dir}")"
		if [ "${ref}" != "${remote_ref}" ]; then
			echo "Remote's don't match. Reinstalling ${component_type} '${component_name}'"
			command rm -r "${target_dir}"
			_shcfg__install_component "${ref}" "${target_dir}" "${component_type}" "${component_name}"
		fi
	else
		[ "${SHCFG_DEBUG}" = 'true' ] && echo "[DEBUG] Installing ${component_type} '${component_name}'"
		_shcfg__install_component "${ref}" "${target_dir}" "${component_type}" "${component_name}"
	fi
}
_shcfg__install_component() {
	local ref target_dir component_type component_name ec
	ref="$1"
	target_dir="$2"
	component_type="$3"
	component_name="$4"
	ec=0
	if _shcfg__git_clone "${ref}" "${target_dir}"; then
		[ "${SHCFG_DEBUG}" = 'true' ] && echo "[DEBUG] Install successful: ${component_type} '${component_name}'"
	else
		ec=$?
		[ "${SHCFG_DEBUG}" = 'true' ] && echo "[DEBUG] Install failed: ${component_type} '${component_name}'"
	fi
	return "${ec}"
}

# module name resolution
_shcfg__resolve_module_name() {
	local raw_module_name="$1"
	if _shcfg__is_valid_alias "${raw_module_name}" >/dev/null 2>&1; then
		if _shcfg__get_module_by_alias "${raw_module_name}"; then
			return $?
		fi
	fi
	_shcfg__expand_module_name "${raw_module_name}"
}
_shcfg__expand_module_name() {
	local raw_module_name module repo ec
	raw_module_name="$1"
	ec=0
	_result=
	_shcfg__is_module_name_valid "${raw_module_name}" || return $?
	case "${raw_module_name}" in
		(*/*)
			if _shcfg__is_module_indexed "${raw_module_name}"; then
				_result="${raw_module_name}"
			else
				echo "[ERROR] Unknown module: '${raw_module_name}'!"
				ec=1
			fi
		;;
		(*)
			module="${raw_module_name}"
			_shcfg__get_repo_for_module "${module}" || return $?
			repo="${_result}"
			_result="${repo}/${module}"
		;;
	esac
	return "${ec}"
}
_shcfg__get_repo_for_module() {
	local module="$1" repo
	_shcfg__get_repos_for_module "${module}" || return $?
	if [ ${#_result[@]} -eq 0 ]; then
		echo "[ERROR] Module '${module}' is unknown ... ignoring" >&2
		_result=
		return 1
	elif [ ${#_result[@]} -eq 1 ]; then
		repo="${_result[0]}"
		_result=( "${repo}" )
		return 0
	else
		for repo in "${_result[@]}"; do
			if [ -z "${repos}" ]; then
				repos="${repo}"
			else
				repos+=", ${repo}"
			fi
		done
		echo "[ERROR] Module '${module}' is ambiguous. Please specify which one you'd like to include!" >&2
		echo "[ERROR] Following repos contain a module named '${module}': ${repos}" >&2
		_result=
		return 1
	fi
}
_shcfg__get_repos_for_module() {
	_result=()
	local module="$1" index_module repo
	for index_module in "${!_SHCFG_MODULE_INDEX[@]}"; do
		repo="${index_module%%/*}"
		index_module="${index_module#*/}"
		if [[ $index_module == "$module" ]]; then
			_result+=( "${repo}" )
		fi
	done
}
_shcfg__is_module_name_valid() {
	local module_name="$1" ec=0
	case "${module_name}" in
		# auto-extracted module names may contain whitespace!
		(*${LF}*)
			echo "[ERROR] Module names may not contain line breaks! (${module_name})"
			ec=1
		;;
		(*/*/*)
			echo "[ERROR] Invalid module name: '${module_name}'." \
			     "Module names have to look like: [<repo>/]<module>"
			ec=1
		;;
	esac
	return "${ec}"
}
