#!/bin/bash
# Copyright 2020 Andanan
#
# This file is part of shcfg, a configuration management framework
# for the bash shell. For more information take a look at
# <https://gitlab.com/shcfg/shcfg>.
#
# shcfg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# shcfg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
# This file contains helper functions to interact with the underlying
# data structure in a shell agnostic way

# define global data structures
declare -Ag _SHCFG_REPO_INDEX=()     # <repo>:<ref>
declare -Ag _SHCFG_MODULE_INDEX=()   # <repo>/<module>:<ref>
declare -Ag _SHCFG_MODULE_ALIASES=() # <alias>:<repo>/<module>
declare -ag _SHCFG_ACTIVE_MODULES=() # <repo>/<module>


# data helpers
_shcfg__for_each_dataset_entry() {
	# Usage: _shcfg__for_each_module [ENTRIES...] --- [OPTIONS...] CALLBACK [ARGS...]
	# Options: --collect     collect results of callbacks returning zero
	#          --break-fast  stop looping when a callback-ec (mis)matches condition
	#          --all         expect all callbacks to return with zero-ec (default)
	#          --none        expect all callbacks to return non-zero
	#          --any         expect at least one callback to return with zero-ec
	#          --any-not     expect at least one callback to return non-zero
	# --break-fast has different meanings for different conditions:
	#   - for `--all` it fails as soon as a callback returns non-zero
	#   - for `--none` it fails as soon as a callback returns a zero ec
	#   - for `--any` it succeeds as soon as a callback returns a zero ec
	#   - for `--any-not` it succeeds as soon as a callback returns non-zero
	# IMPORTANT:
	#   - if any of the ENTRIES may contain a newline char, set the environment
	#     variable _DATASET_IFS to a character that is definitely not contained
	#     in any ENTRY!
	#   - if any of the ENTRIES may be the entry terminator '---' (three dashes)
	#     you can override it by specifying _DATASET_TERMINATOR!

	local entries _ifs _oifs dataset_terminator collect break_fast success_condition \
	      callback module lines ec
	_ifs="${_DATASET_IFS:-"${LF}"}"
	_oifs="${IFS}"
	dataset_terminator="${_DATASET_TERMINATOR:-'---'}"
	collect='false'
	break_fast='false'
	success_condition='all'
	callback=
	lines=
	ec=0
	_result=
	while [ $# -gt 0 -a "$1" != "${dataset_terminator}" ]; do
		if [ -z "${entries}" ]; then
			entries="$1"
		else
			entries="${entries}${_ifs}$1"
		fi
		shift
	done
	[ "$1" = "${dataset_terminator}" ] && shift
	while [ $# -gt 0 ]; do
		case "$1" in
			(--collect) collect='true';;
			(--break-fast) break_fast='true';;
			(--all) success_condition='all';;
			(--none) success_condition='none';;
			(--any) success_condition='any';;
			(--any-not) success_condition='any-not';;
			(*-)
				echo "[ERROR] Unknown option: $1" >&2
				return 1
			;;
			(*)
				callback="$1"
			;;
		esac
		shift
		[ -n "${callback}" ] && break
	done
	case "${success_condition}" in
		# inverted success condition!
		(any|any-not) ec=1;;
	esac
	IFS="${_ifs}"
	for entry in ${entries}; do
		IFS="${_oifs}"
		"${callback}" "${entry}" "$@"
		if [ $? -eq 0 ]; then
			if [ "${collect}" = 'true' ]; then
				lines="${lines}${_result}${LF}"
			fi
			if [ "${success_condition}" = 'any' ]; then
				ec=0
				[ "${break_fast}" = 'true' ] && break
			elif [ "${success_condition}" = 'none' ]; then
				ec=1
				[ "${break_fast}" = 'true' ] && break
			fi
		else
			if [ "${success_condition}" = 'any-not' ]; then
				ec=0
				[ "${break_fast}" = 'true' ] && break
			elif [ "${success_condition}" = 'all' ]; then
				ec=1
				[ "${break_fast}" = 'true' ] && break
			fi
		fi
	done
	_result="${lines}"
	return "${ec}"
}


# repo index helpers
_shcfg__add_repo_to_index() {
	local name="$1" ref="$2" ec=0 other_ref
	if ! _shcfg__is_repo_indexed "${name}"; then
		_SHCFG_REPO_INDEX["${name}"]="${ref}"
	else
		_shcfg__get_repo_ref "${name}"
		other_ref="${_result}"; _result=
		if [ "${ref}" != "${other_ref}" ]; then
			echo "[WARN]  Repo '$name' is already indexed with another reference!" >&2
			echo "[WARN]  Keep old reference: ${other_ref}. (Ignore new one: ${ref})" >&2
		fi
	fi
	return "${ec}"
}
_shcfg__get_repo_ref() {
	local repo="$1" ec=1
	_result=
	if _shcfg__is_repo_indexed "${repo}"; then
		_result="${_SHCFG_REPO_INDEX["${repo}"]}"
		ec=0
	fi
	return "${ec}"
}
_shcfg__is_repo_indexed() {
	local repo="$1"
	[ -n "${_SHCFG_REPO_INDEX["${repo}"]}" ]
}
_shcfg__for_each_repo() {
	# set data terminator to linefeed because repo names may not contain that
	_DATASET_TERMINATOR="${LF}" \
	_shcfg__for_each_dataset_entry "${!_SHCFG_REPO_INDEX[@]}" "${LF}" "$@"
}

# module index helpers
_shcfg__add_module_to_index() {
	local module="$1" ref="$2" ec=0 other_ref
	if ! _shcfg__is_module_indexed "${module}"; then
		_SHCFG_MODULE_INDEX["${module}"]="${ref}"
	else
		_shcfg__get_module_ref "${module}"
		other_ref="${_result}"; _result=
		if [ "${ref}" != "${other_ref}" ]; then
			echo "[WARN]  Module '${module}' is already indexed with another reference!" >&2
			echo "[WARN]  Keep old reference: ${other_ref}. (Ignore new one: ${ref})" >&2
		fi
	fi
	return "${ec}"
}
_shcfg__get_module_ref() {
	local module="$1" ec=1
	_result=
	if _shcfg__is_module_indexed "${module}"; then
		_result="${_SHCFG_MODULE_INDEX["${module}"]}"
		ec=0
	fi
	return "${ec}"
}
_shcfg__is_module_indexed() {
	local module="$1"
	[ -n "${_SHCFG_MODULE_INDEX["${module}"]}" ]
}
_shcfg__for_each_module() {
	# set data terminator to linefeed because module names may not contain that
	_DATASET_TERMINATOR="${LF}" \
	_shcfg__for_each_dataset_entry "${!_SHCFG_MODULE_INDEX[@]}" "${LF}" "$@"
}

# module alias helpers
_shcfg__add_alias() {
	local alias="$1" module="$2" ec=0 other_module
	if ! _shcfg__is_alias "${alias}"; then
		_SHCFG_MODULE_ALIASES["${alias}"]="${module}"
	else
		_shcfg__get_module_by_alias "${alias}"
		other_module="${_result}"; _result=
		if [ "${ref}" != "${other_module}" ]; then
			echo "[WARN]  Alias '${alias}' already defined for another module!" >&2
			echo "[WARN]  Keep referencing old module '${other_module}'. (Ignore new one: '${module}')" >&2
		fi
	fi
	return "${ec}"
}
_shcfg__get_alias() {
	local module="$1" ec=1 alias
	_result=
	for alias in "${!_SHCFG_MODULE_ALIASES[@]}"; do
		if [ "${_SHCFG_MODULE_ALIASES["${alias}"]}" = "${module}" ]; then
			ec=0
			_result="${alias}"
			break
		fi
	done
	return "${ec}"
}
_shcfg__get_module_by_alias() {
	local alias="$1" ec=1
	_result=
	if _shcfg__is_alias "${alias}"; then
		_result="${_SHCFG_MODULE_ALIASES["${alias}"]}"
		ec=0
	fi
	return "${ec}"
}
_shcfg__is_alias() {
	local alias="$1"
	[ -n "${_SHCFG_MODULE_ALIASES["${alias}"]}" ]
}
_shcfg__is_valid_alias() {
	local alias="$1" ec=0
	case "${alias}" in
		(*[[:space:]]*)
			echo "[WARN] Module aliases may not contain any whitespace!" >&2
			ec=1
		;;
		(*/*)
			echo "[WARN] Module aliases may not contain any slashes!" >&2
			ec=1
		;;
	esac
	return "${ec}"
}

# active module index helpers
_shcfg__add_module_to_active_index() {
	local module="$1" ec=0
	if ! _shcfg__is_module_active "${module}"; then
		_SHCFG_ACTIVE_MODULES+=( "${module}" )
	fi
	return "${ec}"
}
_shcfg__remove_module_from_active_index() {
	local module="$1" activated_module activated_modules
	activated_modules=()
	for activated_module in "${_SHCFG_ACTIVE_MODULES[@]}"; do
		if [ "${activated_module}" != "${module}" ]; then
			activated_modules+=( "${activated_module}" )
		fi
	done
	_SHCFG_ACTIVE_MODULES=( "${activated_modules[@]}" )
}
_shcfg__is_module_active() {
	local query="$1" module
	for module in "${_SHCFG_ACTIVE_MODULES[@]}"; do
		if [[ ${query} == "${module}" ]]; then
			return 0
		fi
	done
	return 1
}
