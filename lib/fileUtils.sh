#!/bin/sh
# Copyright 2020 Andanan
#
# This file is part of shcfg, a configuration management framework
# for the bash shell. For more information take a look at
# <https://gitlab.com/shcfg/shcfg>.
#
# shcfg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# shcfg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# io utils
_shcfg__read_file() {
	# Usage:
	#   _shcfg__read_file [OPTIONS...] FILE CALLBACK [ARGS...]
	# Options:
	#   --no-skip-blank       Do not skip empty lines
	#   --no-skip-comments    Do not skip lines starting with '#'
	#   --no-trim             Do not trim-off leading and trailing whitespace
	#                         (does not affect detection of blank or comment lines)
	#   --fail-fast           Stop reading file after first callback fails or succeeds
	#                         (depends on --all-success or --all-fail)
	#   --all-success         Expect all CALLBACK calls to be successful
	#   --all-fail            Expect all CALLBACK calls to fail
	# Description:
	#   Reads a file line by line and executes CALLBACK for each line. The callback
	#   is called with follwing arguments:
	#     (1) The FILE
	#     (2) The line number of the current line
	#     (3) The actual line content
	#     (4+) Any ARGS specified after the CALLBACK
	#   Per default leading and trailing whitespace is trimmed off from lines, lines
	#   starting with '#' are treated as comments and therefore ignored, empty lines
	#   are ignored (lines containing only whitespace are emptied by the trimming)
	#   and the result of callback calls is not checked.
	# Returns:
	#   The _result var is always empty
	#   ec=0 if callback results are ignored (default) or did match disired mode:
	#     --all-success: all callback calls were successful
	#     --all-fail: all callback calls did fail
	#   ec=1 if callback results did not match the desired mode:
	#     --all-success: at least one callback call failed
	#     --all-fail: at least one callback call was successful
	#   ec=2 if an unknown option is specified
	#   ec=3 if the FILE argument is missing or empty
	#   ec=4 if the file denoted by FILE does not exist
	#   ec=5 if the file denoted by FILE can not be read
	#   ec=6 if the CALLBACK is missing or empty
	#   ec=7 if the CALLBACK is not a command or function
	local file callback skip_comments skip_blank_lines trim fail_fast line trimmed_line \
	      line_number ec callback_ec condition
	skip_comments='true'
	skip_blank_lines='true'
	trim='true'
	fail_fast='false'
	line_number=0
	ec=0
	condition='no-check'

	# parse arguments
	while [ $# -gt 0 ]; do
		case "$1" in
			(--no-skip-blank)    skip_blank_lines='false';;
			(--no-skip-comments) skip_comments='false';;
			(--fail-fast)        fail_fast='true';;
			(--no-trim)          trim='false';;
			(--all-success)      condition='success';;
			(--all-fail)         condition='fail';;
			(-*)
				echo "[ERROR] Unknown option: $1" >&2
				return 2
			;;
			(*)
				if [ -z "${file}" ]; then
					file="$1"
				elif [ -z "${callback}" ]; then
					callback="$1"
				else
					# any remaining args will be passed to callback
					break
				fi
			;;
		esac
		shift
	done
	if [ -z "${file}" ]; then
		echo "[ERROR] File to read is missing!" >&2
		ec=3
	elif [ ! -f "${file}" ]; then
		echo "[ERROR] Not a file: ${file}" >&2
		ec=4
	elif [ ! -r "${file}" ]; then
		echo "[ERROR] File can not be read: ${file}" >&2
		ec=5
	elif [ -z "${callback}" ]; then
		echo "[ERROR] Callback is missing!" >&2
		ec=6
	elif ! command -v "${callback}" >/dev/null 2>&1; then
		echo "[ERROR] Not a command nor a function: ${callback}" >&2
		ec=7
	else
		# read the file
		# shellcheck disable=SC2094
		#   (callback functions use 'file' only for error messages)
		# TODO: check if we can get rid of the FILE and LINE_NO parameters (global var?)
		while IFS= read -r line || [ -n "${line}" ]; do
			line_number=$((line_number + 1))
			_string_utils__trim "${line}"
			trimmed_line="${_result}"; _result=
			case "${trimmed_line}" in # (special lines)
				(\#*)
					[ "${skip_comments}" = 'true' ] && continue
				;;
				('')
					[ "${skip_blank_lines}" = 'true' ] && continue
				;;
			esac
			if [ "${trim}" = 'true' ]; then
				line="${trimmed_line}"
			fi
			# append additional args for more flexibility
			"${callback}" "${file}" "${line_number}" "${line}" "$@"
			callback_ec=$?
			if [ "${callback_ec}" -gt 0 -a "${condition}" = 'success' ] ||
			   [ "${callback_ec}" -eq 0 -a "${condition}" = 'fail' ]; then
				ec=1
				[ "${fail_fast}" = 'true' ] && break
			fi
		done < "${file}"
	fi
	_result=
	return "${ec}"
}

# parser utils
_shcfg__cfg__get_name() {
	# get entry name either from alias or from reference
	local line="$1" ec=0 alias ref name
	[ -z "${line}" ] && return 1
	# get alias
	_shcfg__cfg__get_alias "${line}" || return $?
	alias="${_result}"; _result=

	if [ -n "${alias}" ]; then
		name="${alias}"
	else
		# try to extract the name from the URL/path
		_shcfg__cfg__get_ref "${line}" || return $?
		name="${_result%/}"; _result=
		name="${name##*/}"
		name="${name%.git}"
	fi
	[ -z "${name}" ] && ec=1
	_result="${name}"
	return "${ec}"
}
_shcfg__cfg__get_alias() {
	# Searches for alias sections in a config entry line-string and assigns the
	# value of the last alias-section it finds before the start of the reference
	# to '_result'. If no alias is found, '_result' will be empty. The exit code
	# will be 0 in any case, because aliases (like the other sections) are optional.
	local entry="$1" alias
	while [ -n "${entry}" ]; do
		case "${entry}" in
			(@*)
				alias="${entry%%[[:space:]]*}"
				alias="${alias#@}"
			;;
			(@*|:*|+*|!*);; # ignore other sections
			(*)
				# ref begins
				break
			;;
		esac
		# strip-off metadata section
		entry="${entry#${entry%%[[:space:]]*}}"
		# strip-off leading whitespace
		_string_utils__ltrim "${entry}"
		entry="${_result}"
	done
	_result="${alias}"
}
_shcfg__cfg__get_ref() {
	# Searches for a reference in a config entry line-string. If a reference is
	# found, '_result' will contain the reference and the exit code will be 0. If
	# no reference is found, '_result' will be empty and the exit code will be 1.
	local entry="$1" ec=1 ref
	while [ -n "${entry}" ]; do
		case "${entry}" in
			(@*|:*|+*|!*);; # ignore metadata sections
			(*)
				ref="${entry}"
				ec=0
				break
			;;
		esac
		# strip-off metadata section
		entry="${entry#${entry%%[[:space:]]*}}"
		# strip-off leading whitespace
		_string_utils__ltrim "${entry}"
		entry="${_result}"
	done
	_result="${ref}"
	return "${ec}"
}
_shcfg__cfg__parse_inclusion() {
	local config_file line_num line parser_func dir pattern opwd oifs file
	config_file="$1"
	line_num="$2"
	line="$3"
	parser_func="$4"
	opwd="${PWD}"
	oifs="${IFS}"
	dir="$(dirname "${config_file}")"
	# get pattern
	if _shcfg__cfg__get_ref "${line}"; then
		pattern="${_result}"; _result=
	else
		_string_utils__prettify_path --home "${config_file}"
		echo "[ERROR] Include failed: globbing pattern missing! (${_result}:${line_num})" >&2
		_result=
		return 1
	fi
	[ "${SHCFG_DEBUG}" = 'true' ] && echo "[DEBUG] Include pattern = '${pattern}'"
	# change dir to allow relative paths
	cd "${dir}"
	# unset IFS to allow spaces in globbing pattern (https://unix.stackexchange.com/a/264300)
	IFS=
	# no quotes to allow glob-pattern
	for file in ${pattern}; do
		if [ -e "${file}" ]; then
			[ -d "${file}" ] && continue
			"${parser_func}" "${file}"
		elif [ -z "${file}" -o "${file}" = "${pattern}" ]; then
			[ "${SHCFG_DEBUG}" = 'true' ] && echo "[DEBUG] No matching files for pattern: '${pattern}'"
			break
		else
			echo "[ERROR] This should never happen! (pattern='${pattern}', file='${file}')" >&2
		fi
	done
	IFS="${oifs}"
	cd "${opwd}"
}

# repo config parser
_shcfg__parse_repo_cfg() {
	local repo_conf="$1"
	if [ ! -f "${repo_conf}" ]; then
		if [ "${SHCFG_DEBUG}" = 'true' ]; then
			echo "[DEBUG] Repo config file '${repo_conf}' does not exit! (Skipping)" >&2
		fi
		return
	elif [ "${repo_conf}" != "${SHCFG_REPO_CONF}" ]; then
		if [ "${SHCFG_DEBUG}" = 'true' ]; then
			_string_utils__prettify_path --home "${repo_conf}"
			echo "[INFO]  Load repo config file: ${_result}"
		fi
	fi
	_shcfg__read_file "${repo_conf}" '_shcfg__parse_repo_cfg_line'
}
_shcfg__parse_repo_cfg_line() {
	local repo_conf="$1" line_num="$2" line="$3" repo_name repo_ref
	case "${line}" in
		(!include|!include[[:space:]]*)
			_shcfg__cfg__parse_inclusion "${repo_conf}" \
			                             "${line_num}" \
			                             "${line}" \
			                             '_shcfg__parse_repo_cfg'
			return $?
		;;
	esac
	# get repository name
	_shcfg__cfg__get_name "$line" || return 1
	repo_name="${_result}"; _result=
	# get repository reference
	_shcfg__cfg__get_ref "${line}" || return 1
	repo_ref="${_result}"; _result=
	# add repository to index
	_shcfg__add_repo_to_index "${repo_name}" "${repo_ref}"
}

# module repo parser
_shcfg__parse_repo_modules() {
	local repo="$1" module_list
	module_list="${_SHCFG_REPOS_DIR}/${repo}/modules.list"
	if [ ! -f "${module_list}" ]; then
		echo "Module repo '${repo}' doesn't contain a 'modules.list' file!" >&2
		return 1
	fi
	_shcfg__read_file "${module_list}" '_shcfg__parse_repo_modules_line' "${repo}"
}
_shcfg__parse_repo_modules_line() {
	local line="$3" repo="$4" module_name module_ref
	# get module name
	_shcfg__cfg__get_name "$line" || return 1
	module_name="${_result}"; _result=
	# get module reference
	_shcfg__cfg__get_ref "${line}" || return 1
	module_ref="${_result}"; _result=
	# add module to index
	module_name="${repo}/${module_name}"
	_shcfg__add_module_to_index "${module_name}" "${module_ref}"
}

# module config parser
_shcfg__parse_module_cfg() {
	local module_conf="$1"
	if [ ! -f "${module_conf}" ]; then
		if [ "${SHCFG_DEBUG}" = 'true' ]; then
			echo "[DEBUG] Module config file '${module_conf}' does not exit! (Skipping)" >&2
		fi
		return
	elif [ "${module_conf}" != "${SHCFG_MODULE_CONF}" ]; then
		if [ "${SHCFG_DEBUG}" = 'true' ]; then
			_string_utils__prettify_path --home "${module_conf}"
			echo "[INFO]  Load module config file: ${_result}"
		fi
	fi
	_shcfg__read_file "${module_conf}" "_shcfg__parse_module_cfg_line"
}
_shcfg__parse_module_cfg_line() {
	local file="$1" line_num="$2" line="$3" module alias module_ref
	case "${line}" in
		(!include|!include[[:space:]]*)
			_shcfg__cfg__parse_inclusion "${file}" \
			                             "${line_num}" \
			                             "${line}" \
			                             '_shcfg__parse_module_cfg'
			return $?
		;;
	esac
	# get alias
	_shcfg__cfg__get_alias "${line}"
	alias="${_result}"; _result=
	# get module reference
	_shcfg__cfg__get_ref "${line}"
	module_ref="${_result}"; _result=
	# expand module reference if necessary
	_shcfg__expand_module_name "${module_ref}" || return $?
	module="${_result}"; _result=
	# mark module as activated
	_shcfg__add_module_to_active_index "${module}" || return $?
	# activate alias if set
	if [ -n "${alias}" ]; then
		if _shcfg__is_valid_alias "${alias}"; then
			_shcfg__add_alias "${alias}" "${module}"
		fi
	fi
}

# module config update
_shcfg__add_module_to_cfg() {
	local module ec do_print
	module="$1"
	ec=0
	do_print='true'
	if [ -f "${SHCFG_MODULE_CONF}" ]; then
		_shcfg__read_file --all-fail \
		                  "${SHCFG_MODULE_CONF}" \
		                  '_shcfg__is_module_configured_in_line' \
		                  "${module}" || {
			if [ $? -eq 1 ]; then
				# module is already configured
				if [ "${SHCFG_DEBUG}" = 'true' ]; then
					echo "[DEBUG] Module already in module config: '${module}'" >&2
				fi
			else
				# read file failed
				ec=1
			fi
			do_print='false'
		}
	fi
	if [ "${do_print}" = 'true' ]; then
		echo "${module}" >> "${SHCFG_MODULE_CONF}" || ec=$?
	fi
	return "${ec}"
}
_shcfg__is_module_configured_in_line() {
	local line module activated_module module_ref
	line="$3"
	module="$4"
	case "${line}" in
		# skip comments, inclusions and empty lines
		(\#*|!include|!include[[:space:]]*|'') return 0;;
	esac
	# get module reference
	_shcfg__cfg__get_ref "${line}"
	module_ref="${_result}"; _result=
	# expand module reference if necessary
	_shcfg__expand_module_name "${module_ref}" 2>/dev/null || return $?
	activated_module="${_result}"; _result=
	# check if module is configured in current line
	[ "${module}" = "${activated_module}" ]
}
_shcfg__remove_module_from_cfg() {
	if [ -f "${SHCFG_MODULE_CONF}" ]; then
		local module tmp_cfg line activated_module
		module="$1"
		tmp_cfg="$(mktemp)"
		_shcfg__read_file --no-skip-comments \
		                  --no-skip-blank \
		                  --no-trim \
		                  "${SHCFG_MODULE_CONF}" \
		                  '_shcfg__print_new_module_cfg_line' \
		                  "${module}" \
		                  "${tmp_cfg}"
		command mv "${tmp_cfg}" "${SHCFG_MODULE_CONF}"
	fi
}
_shcfg__print_new_module_cfg_line() {
	local line module tmp_cfg activated_module do_print module_ref trimmed_line
	line="$3"
	module="$4"
	tmp_cfg="$5"
	do_print='false'
	# check trimmed line, but store raw one
	_string_utils__trim "${line}"
	trimmed_line="${_result}"; _result=
	case "${trimmed_line}" in
		(\#*|''|!include|!include[[:space:]]*)
			do_print='true'
		;;
		(*)
			# get module reference
			_shcfg__cfg__get_ref "${trimmed_line}"
			module_ref="${_result}"; _result=
			# expand module reference if necessary
			_shcfg__expand_module_name "${module_ref}" 2>/dev/null || return $?
			activated_module="${_result}"; _result=
			# check if lines needs to be deleted (= not printed)
			if [ "${module}" != "${activated_module}" ]; then
				do_print='true'
			fi
		;;
	esac
	if [ "${do_print}" = 'true' ]; then
		echo "${line}" >> "${tmp_cfg}"
	fi
}
