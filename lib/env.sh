#!/bin/sh
# Copyright 2020 Andanan
#
# This file is part of shcfg, a configuration management framework
# for the bash shell. For more information take a look at
# <https://gitlab.com/shcfg/shcfg>.
#
# shcfg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# shcfg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# control characters
[ -z "${CR}" ] && readonly CR="$(printf '\r')"
[ -z "${LF}" ] && readonly LF="
"
[ -z "${ESC}" ] && readonly ESC="$(printf '\033')"
[ -z "${TAB}" ] && readonly TAB='	'

if [ -z "${_SHCFG_VERSION}" ]; then
	# special values
	readonly _SHCFG_VERSION="0.1.0"

	# default values
	readonly _SHCFG_DEFAULT_CONF_DIR="${XDG_CONFIG_HOME:-${HOME}/.config}/shcfg"
	readonly _SHCFG_DEFAULT_DATA_DIR="${XDG_DATA_HOME:-${HOME}/.local/share}/shcfg"
fi

# config dir
if [ -z "${SHCFG_CONF_DIR}" -o ! -d "${SHCFG_CONF_DIR}" ]; then
	SHCFG_CONF_DIR="${_SHCFG_DEFAULT_CONF_DIR}"
fi

# config files
if [ -z "${SHCFG_REPO_CONF}" -o ! -f "${SHCFG_REPO_CONF}" ]; then
	SHCFG_REPO_CONF="${SHCFG_CONF_DIR}/repositories.conf"
fi
if [ -z "${SHCFG_MODULE_CONF}" -o ! -f "${SHCFG_MODULE_CONF}" ]; then
	SHCFG_MODULE_CONF="${SHCFG_CONF_DIR}/modules.conf"
fi

# data dirs
if [ -z "${SHCFG_DATA_DIR}" -o ! -d "${SHCFG_DATA_DIR}" ]; then
	SHCFG_DATA_DIR="${_SHCFG_DEFAULT_DATA_DIR}"
fi
if [ -z "${_SHCFG_REPOS_DIR}" ]; then
	readonly _SHCFG_REPOS_DIR="${SHCFG_DATA_DIR}/repositories"
	readonly _SHCFG_MODULES_DIR="${SHCFG_DATA_DIR}/modules"
fi
