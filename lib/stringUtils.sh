#!/bin/sh
# Copyright 2020 Andanan
#
# This file is part of shcfg, a configuration management framework
# for the bash shell. For more information take a look at
# <https://gitlab.com/shcfg/shcfg>.
#
# shcfg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# shcfg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# trim (https://stackoverflow.com/a/3352015)
_string_utils__ltrim() {
	# trim leading whitespace
	local str="$1"
	_result="${str#"${str%%[![:space:]]*}"}"
}
_string_utils__rtrim() {
	# trim trailing whitespace
	local str="$1"
	_result="${str%"${str##*[![:space:]]}"}"
}
_string_utils__trim() {
	# trim leading and trailing whitespace
	_string_utils__ltrim "$1" && \
	_string_utils__rtrim "${_result}"
}

# path utils
_string_utils__abs_path() {
	# Usage: _string_utils__abs_path PATH
	# makes sure that PATH is absolute
	local path
	if [ -n "$1" ]; then
		path="$1"
	else
		echo "[ERROR] PATH is missing" >&2
		echo "[ERROR] Usage: _string_utils__abs_path PATH" >&2
		_result=
		return 1
	fi
	case "${path}" in
		(/*);; # path already absolute
		(*)
			if [ -n "${PWD}" ]; then
				path="${PWD}/${path}"
			else
				path="$(pwd)/${path}"
			fi
		;;
	esac
	_result="${path}"
	return 0
}
_string_utils__cleanup_path() {
	# Usage: _string_utils__cleanup_path PATH
	# Resolves any '.' and '..' sections in PATH. PATH is expected to be absolute
	# to handle paths like '../lorem' correctly.
	local path tmp_path inline skip pop_dir pop_slash section
	if [ -n "$1" ]; then
		path="$1"
	else
		echo "[ERROR] PATH is missing" >&2
		echo "[ERROR] Usage: _string_utils__cleanup_path PATH" >&2
		_result=
		return 1
	fi
	case "${path}" in
		(*/..|*/../*|*/.|*/./*)
			: # path is dirty
		;;
		(*)
			# path is already clean
			_result="${path}"
			return 0
		;;
	esac
	tmp_path='/'
	path="${path#/}"
	while [ -n "${path}" ]; do
		inline='true'
		skip='false'
		pop_dir='false'
		pop_slash='false'
		# get current section
		section="${path%%/*}"

		# analyze current section
		if [ "${section}" = "${path}" ]; then
			# is last section
			inline='false'
		fi
		if [ "${section}" = '.' ]; then
			skip='true'
			# pop '/' from parent dir if is last section
			[ "${inline}" = 'false' ] && pop_slash='true'
		fi
		if [ "${section}" = '..' ]; then
			skip='true'
			pop_dir='true'
			# pop '/' from grand-parent dir if is last section
			[ "${inline}" = 'false' ] && pop_slash='true'
		fi

		# inline sections are terminated by a '/'
		[ "${inline}" = 'true' ] && section="${section}/"
		if [ "${skip}" = 'true' ]; then
			if [ "${tmp_path}" != '/' -a "${pop_dir}" = 'true' ]; then
				# pop parent dir
				tmp_path="${tmp_path%/}"
				tmp_path="${tmp_path%${tmp_path##*/}}"
			fi
			if [ "${tmp_path}" != '/' -a "${pop_slash}" = 'true' ]; then
				# pop '/' from parent dir
				tmp_path="${tmp_path%/}"
			fi
		else
			tmp_path="${tmp_path}${section}"
		fi

		# remove current section from raw path
		path="${path#${section}}"
	done
	_result="${tmp_path}"
	return 0
}
_string_utils__prettify_path() {
	# Usage: _string_utils__prettify_path [(--cwd|--home)] PATH
	# Makes the given path prettier by cleaning it up (removing '.' and '..')
	# and making it relative to either HOME or PWD.
	local mode rel_path path rel_prefix
	mode='cwd'

	# parse args
	case "$1" in
		(--cwd) mode='cwd'; shift;;
		(--home) mode='home'; shift;;
	esac
	if [ -n "$1" ]; then
		path="$1"
	else
		echo "[ERROR] PATH is missing" >&2
		echo "[ERROR] Usage: _string_utils__prettify_path [(--cwd|--home)] PATH" >&2
		return 1
	fi
	if [ "${mode}" = 'cwd' ]; then
		if [ -n "${PWD}" ]; then
			rel_path="${PWD}"
		else
			rel_path="$(pwd)"
		fi
		rel_prefix='.'
	elif [ "${mode}" = 'home' ]; then
		rel_path="${HOME}"
		rel_prefix='~'
	fi

	# make path absolute
	_string_utils__abs_path "${path}"
	path="${_result}"

	# resolve '.' and '..'
	_string_utils__cleanup_path "${path}"
	path="${_result}"

	# replace leading HOME with '~'
	if [ "${path#${rel_path}}" != "${path}" ]; then
		path="${rel_prefix}${path#${rel_path}}"
	fi
	_result="${path}"
}
