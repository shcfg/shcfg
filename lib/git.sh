#!/bin/sh
# Copyright 2020 Andanan
#
# This file is part of shcfg, a configuration management framework
# for the bash shell. For more information take a look at
# <https://gitlab.com/shcfg/shcfg>.
#
# shcfg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# shcfg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

_shcfg__git() {
	local git_cmd git_args
	git_cmd="$(command -v git)"
	if [ -z "$git_cmd" ]; then
		echo "Action failed: command 'git' not found!" >&2
		return 1
	else
		LC_ALL=en_US "$git_cmd" ${git_args} "$@"
	fi
}

_shcfg__git_clone() {
	local ref="$1" dir="$2" git_args clone_args submodule_args
	[ "${SHCFG_DEBUG}" = 'true' ] || git_args="--quiet"
	clone_args="${git_args} --recurse-submodules"
	clone_args="${clone_args} --no-hardlinks"
	_shcfg__git clone ${clone_args} "${ref}" "${dir}" >&2 || return $?
	(
		cd "${dir}"
		submodule_args="${git_args} --init"
		submodule_args="${submodule_args} --recursive"
		_shcfg__git submodule update ${submodule_args} >&2
	)
}

_shcfg__print_git_remote_url() {
	local repo_dir remote
	repo_dir="$1"
	remote="${2:-origin}"
	if [ ! -d "${repo_dir}" ]; then
		echo "[ERROR] Not a dir: ${repo_dir}" >&2
		return 1
	fi
	GIT_CONFIG="${repo_dir}/.git/config" \
	_shcfg__git config --get "remote.${remote}.url"
}
