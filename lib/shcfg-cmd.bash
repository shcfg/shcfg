#!/bin/bash
# Copyright 2020 Andanan
#
# This file is part of shcfg, a configuration management framework
# for the bash shell. For more information take a look at
# <https://gitlab.com/shcfg/shcfg>.
#
# shcfg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# shcfg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

shcfg() {
	local mode ec=0
	if [ $# -eq 0 ]; then
		_shcfg__print_err_usage 'shcfg'
		return 1
	else
		mode="$1"
		shift
	fi
	case "${mode}" in
		(help|list|install|uninstall)
			"_shcfg-${mode}" "$@"
			ec=$?
		;;
		(reload)
			_shcfg__init
			ec=$?
		;;
		(-h|--help|-?)
			_shcfg-help "$@"
			ec=$?
		;;
		(--license)
			cat "${SHCFG_HOME}/LICENSE.txt"
		;;
		(--version)
			echo "${_SHCFG_VERSION}"
		;;
		(*)
			printf 'Unknown mode: %s\n\n' "${mode}" >&2
			_shcfg__print_err_usage
			ec=1
		;;
	esac
	return "${ec}"
}

# print usage
_shcfg__print_usage() {
	local mode cmd
	mode="$1"
	cmd="_shcfg__print_usage__${mode}"
	if command -v "${cmd}" >/dev/null 2>&1; then
		"${cmd}"
	else
		echo "Usage: shcfg ${mode} [ARGS...]"
	fi
	echo
}
_shcfg__print_usage__shcfg() {
	echo "Usage: shcfg MODE [ARGS...]"
	echo "       shcfg [OPTION]"
}
_shcfg__print_usage__reload() {
	echo "Usage: shcfg reload"
}
_shcfg__print_usage__list() {
	echo "Usage: shcfg list [MODE] [OPTION...]"
}
_shcfg__print_usage__install() {
	echo "Usage: shcfg install MODULE [MODULE...]"
}
_shcfg__print_usage__uninstall() {
	echo "Usage: shcfg uninstall MODULE [MODULE...]"
}
_shcfg__print_err_usage() {
	local mode help_cmd
	mode="$1"
	help_cmd='shcfg help'
	if [ -z "${mode}" -o "${mode}" = 'shcfg' ]; then
		mode='shcfg'
	else
		help_cmd="${help_cmd} ${mode}"
	fi
	_shcfg__print_usage "${mode}" >&2
	echo "For a more detailed help, type: \`${help_cmd}\`" >&2
}

# help mode
_shcfg-help() {
	local ec=0 topic cmd
	if [ $# -eq 0 ]; then
		topic="shcfg"
	else
		topic="$1"
	fi
	cmd="_shcfg__print_help__${topic}"
	if command -v "${cmd}" >/dev/null 2>&1; then
		"${cmd}"
		ec=$?
		echo # always end with empty line
	else
		echo "No help available for topic '${topic}'" >&2
		ec=1
	fi
	return "${ec}"
}
_shcfg__print_help__shcfg() {
	_shcfg__print_usage 'shcfg'
	echo 'Available modes are:'
	echo '  help        if called without args, shows this help message'
	echo '  reload      re-initialize shcfg'
	echo '  list        list modules and module repositories'
	echo '  install     install a module'
	echo '  uninstall   uninstall a module'
	echo
	echo 'To get more help for a mode, type `shcfg help <MODE>`'
	echo
	echo 'Additionally some special options are available:'
	echo '  -h, --help      prevalent options for showing help. Synonyms for `shcfg help`'
	echo '      --license   show the license text'
	echo '      --version   show the version number of your shcfg installation'
}
_shcfg__print_help__reload() {
	_shcfg__print_usage 'reload'
	echo 'Re-initialize shcfg. This is for example useful if you did edit the config files'
	echo 'manually and want shcfg to recognize the changes.'
}
_shcfg__print_help__list() {
	_shcfg__print_usage 'list'
	cat <<- EOF
		List modules and module repositories according to the applied filters.
		Aliases are listed in a separate column, if for the modules mode one or
		more of the listed modules have an active alias. All modules without an
		active alias have the alias column set to '-'. If none of the listed modules
		has an active alias, the alias column is hidden.

		Modes:
		  modules              list modules and theme modules
		  repos                list module repositories

		Module status filters: (module-mode only)
		  -a, --available      show currently not installed modules
		  +a, --no-available   hide currently not installed modules
		  -i, --installed      show currently installed modules
		  +i, --no-installed   hide currently installed modules
		      --all            combines --installed and --available

		Module list options:   (module-mode only)
		  -S|--status          show status column
		  +S|--no-status       hide status column

		If not specified otherwise the following defaults are active:
		  * mode: modules
		  * status filters: installed
		  * module list options:
		    * --status    (if more than one status filter is active)
		    * --no-status (if only one status filter is active)
		Therefore the default action for 'shcfg list' is equivalent to:
		  shcfg list modules --installed --no-status
EOF
}
_shcfg__print_help__install() {
	_shcfg__print_usage 'install'
	echo 'Install one ore more modules. The MODULE specifier is in the form:'
	echo '  [<repo-name>/]<module-name>'
	echo 'The repo name is only necessary if the module name is ambiguous.'
}
_shcfg__print_help_uninstall() {
	_shcfg__print_usage 'uninstall'
	echo 'Uninstall one ore more modules. The MODULE specifier is in the form:'
	echo '  [<repo-name>/]<module-name>'
	echo 'The repo name is only necessary if the module name is ambiguous.'
}

# list mode
_shcfg-list() {
	local mode show_header col_count

	# define defaults
	mode='modules'
	show_header='auto'
	col_count=1

	# parse args
	if [ $# -gt 0 ]; then
		case "$1" in
			(modules|repos)
				mode="$1"
				shift
			;;
			(-*);; # ignore options, use default mode
			(*)
				echo "[ERROR] Unknown list mode: $1" >&2
				echo "[ERROR] Valid modes are:" >&2
				echo "[ERROR]   * modules" >&2
				echo "[ERROR]   * repos" >&2
				return 1
			;;
		esac
	fi

	# render list
	case "${mode}" in
		(modules)
			_shcfg__print_module_list "$@"
		;;
		(repos)
			_shcfg__print_repo_list "$@"
		;;
	esac
}
_shcfg__print_module_list() {
	local show_installed show_available show_alias_col show_status_col status_count \
	      module line lines alias status
	# define defaults
	show_installed='auto'
	show_available='false'
	show_alias_col='auto'
	show_status_col='auto'
	status_count=0
	# parse args
	while [ $# -gt 0 ]; do
		case "$1" in
			# module status filters
			(-a|--available) show_available='true';;
			(+a|--no-available) show_available='false';;
			(-i|--installed) show_installed='true';;
			(+i|--no-installed) show_installed='false';;
			(--all)
				show_available='true'
				show_installed='true'
			;;
			# module list options
			(-S|--status) show_status_col='true';;
			(+S|--no-status) show_status_col='false';;
			(*)
				echo "Unknown option: $1" >&2
				return 1
			;;
		esac
		shift
	done
	# determine status count
	[ "${show_installed}" = 'true' ] && status_count=$((status_count + 1))
	[ "${show_available}" = 'true' ] && status_count=$((status_count + 1))
	# determine whether to show default status or not
	if [ "${show_installed}" = 'auto' ]; then
		if [ "${status_count}" -eq 0 ]; then
			show_installed='true'
			status_count=1
		else
			show_installed='false'
		fi
	fi
	# determine whether to show status column or not
	if [ "${show_status_col}" = 'auto' ]; then
		if [ "${status_count}" -eq 1 ]; then
			show_status_col='false'
		else
			show_status_col='true'
		fi
	fi
	# determine whether to show alias column or not
	if [ "${show_alias_col}" = 'auto' ]; then
		if _shcfg__for_each_module --break-fast \
		                           --none \
		                           _shcfg__is_module_listed_and_has_alias; then
			show_alias_col='false'
		else
			show_alias_col='true'
		fi
	fi
	# determine col_count
	[ "${show_status_col}" = 'true' ] && col_count=$((col_count + 1))
	[ "${show_alias_col}" = 'true' ] && col_count=$((col_count + 1))
	# determine whether to show header or not
	[ "${status_count}" -eq 0 ] && show_header='false'
	if [ "${show_header}" = 'auto' ]; then
		if [ "${col_count}" -eq 1 ]; then
			show_header='false'
		else
			show_header='true'
		fi
	fi
	# render list
	_shcfg__for_each_module --collect _shcfg__render_module_list_line
	lines="$(printf '%s' "${_result}" | sort)"
	# render header
	if [ "${show_header}" = 'true' -a -n "${lines}" ]; then
		line="MODULE_NAME"
		[ "${show_alias_col}" = 'true' ] && line="${line} ALIAS"
		[ "${show_status_col}" = 'true' ] && line="${line} STATUS"
		lines="${line}${LF}${lines}"
	fi
	# print table
	[ -n "${lines}" ] && printf '%s\n' "${lines}" | column -t
}
_shcfg__is_module_listed_and_has_alias() {
	local module="$1" alias ec=1
	if _shcfg__is_module_installed "${module}"; then
		[ "${show_installed}" = 'false' ] && return "${ec}"
	else
		[ "${show_available}" = 'false' ] && return "${ec}"
	fi
	_shcfg__get_alias "${module}"
	ec=$?
	_result=
	return "${ec}"
}
_shcfg__render_module_list_line() {
	local module="$1" status alias line
	_result=
	if _shcfg__is_module_installed "${module}"; then
		[ "${show_installed}" = 'false' ] && return 0
		status='installed'
	else
		[ "${show_available}" = 'false' ] && return 0
		status='available'
	fi
	if [ "${show_alias_col}" = 'true' ] && _shcfg__get_alias "${module}"; then
		alias="${_result}"
	fi
	# build line
	line="${module}"
	if [ "${show_alias_col}" = 'true' ]; then
		if [ -n "${alias}" ]; then
			line="${line} ${alias}"
		else
			line="${line} -"
		fi
	fi
	if [ "${show_status_col}" = 'true' -a -n "${status}" ]; then
		line="${line} ${status}"
	fi
	_result="${line}"
	return 0
}
_shcfg__print_repo_list() {
	local lines
	# determine whether to show header or not
	if [ "${show_header}" = 'auto' ]; then
		if [ "${col_count}" -eq 1 ]; then
			show_header='false'
		else
			show_header='true'
		fi
	fi
	# render list
	_shcfg__for_each_repo --collect _shcfg__render_repo_list_line
	lines="$(printf '%s' "${_result}" | sort)"
	# render header
	if [ "${show_header}" = 'true' ]; then
		lines="REPO_NAME${LF}${lines}"
	fi
	# print result
	[ -n "${lines}" ] && printf '%s\n' "${lines}" | column -t
}
_shcfg__render_repo_list_line() {
	local repo="$1"
	_result="${repo}"
}

# install mode
_shcfg-install() {
	local module ec some_successful some_failed
	ec=0
	some_successful='false'
	some_failed='false'
	while [ $# -gt 0 ]; do
		module="$1"; shift
		if ! _shcfg__resolve_module_name "${module}"; then
			some_failed='true'
			continue
		fi
		module="${_result}"
		_result=
		if ! _shcfg__install_module "${module}"; then
			some_failed='true'
			continue
		fi
		some_successful='true'
	done
	if [ "${some_failed}" = 'true' ]; then
		if [ "${some_successful}" = 'true' ]; then
			ec=1
		else
			ec=2
		fi
	fi
	return "${ec}"
}

# uninstall mode
_shcfg-uninstall() {
	local module ec some_successful some_failed
	ec=0
	some_successful='false'
	some_failed='false'
	while [ $# -gt 0 ]; do
		module="$1"; shift
		if ! _shcfg__resolve_module_name "${module}"; then
			some_failed='true'
			continue
		fi
		module="${_result}"
		_result=
		if ! _shcfg__uninstall_module "${module}"; then
			some_failed='true'
			continue
		fi
		some_successful='false'
	done
	if [ "${some_failed}" = 'true' ]; then
		if [ "${some_successful}" = 'true' ]; then
			ec=1
		else
			ec=2
		fi
	fi
	return "${ec}"
}
