#!/bin/bash
# Copyright 2020 Andanan
#
# This file is part of shcfg, a configuration management framework
# for the bash shell. For more information take a look at
# <https://gitlab.com/shcfg/shcfg>.
#
# shcfg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# shcfg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

_shcfg__init() {
	# define global variables
	# shellcheck disable=SC2034
	_result=

	# define local variables
	local lib repo module alias module_file

	# validate environment and set defaults
	if [[ -z ${SHCFG_HOME} || ! -d ${SHCFG_HOME} ]]; then
		SHCFG_HOME="$(dirname "${BASH_SOURCE[0]}")"
		export SHCFG_HOME
		echo "SHCFG_HOME was not set. Guessed it to be '${SHCFG_HOME}'." >&2
	fi

	# load libs
	for lib in "${SHCFG_HOME}"/lib/*; do
		if [[ -f ${lib} ]]; then
			source "${lib}"
		fi
	done

	# print startup notice
	# TODO: A file as flag is ugly -> Replace by env-var or config entry
	#       (something like `shcfg conf startup-info 'off'`)
	if [ ! -f "${SHCFG_CONF_DIR}/.skip_startup_notice" ]; then
		cat <<- EOF

			#===================================================================#
			# SHCFG  (https://gitlab.com/shcfg/shcfg)                           #
			# Copyright (C) 2020  Andanan                                       #
			# This is free software, and you are welcome to redistribute it     #
			# under the terms of the GNU General Public License version 3 or    #
			# (at your option) any later version of that license.               #
			#     type 'shcfg --license' for details.                           #
			# Please note, that this program comes with ABSOLUTELY NO WARRANTY. #
			#===================================================================#

EOF
		touch "${SHCFG_CONF_DIR}/.skip_startup_notice"
	fi


	# load repo list
	if [[ -f ${SHCFG_REPO_CONF} ]]; then
		_shcfg__parse_repo_cfg "${SHCFG_REPO_CONF}"
	fi
	if [[ ${#_SHCFG_REPO_INDEX[@]} == 0 ]]; then
		echo "[ERROR] No shcfg repositories configured! Please specify some in '${SHCFG_REPO_CONF}'" >&2
		return 1
	fi
	if [ "${SHCFG_DEBUG}" = 'true' ]; then
		echo "[DEBUG] _SHCFG_REPO_INDEX: " >&2
		for repo in "${!_SHCFG_REPO_INDEX[@]}"; do
			echo "[DEBUG]  * [${repo}]=${_SHCFG_REPO_INDEX[${repo}]}" >&2
		done
	fi


	# ensure all repos are installed and correctly set up
	for repo in "${!_SHCFG_REPO_INDEX[@]}"; do
		_shcfg__install_module_repo "${repo}" || continue
		_shcfg__parse_repo_modules "${repo}"
	done
	if [ "${SHCFG_DEBUG}" = 'true' ]; then
		echo "[DEBUG] _SHCFG_MODULE_INDEX:" >&2
		for module in "${!_SHCFG_MODULE_INDEX[@]}"; do
			echo "[DEBUG]  * [${module}]=${_SHCFG_MODULE_INDEX[${module}]}" >&2
		done
	fi


	# load list of activated modules
	if [[ -f ${SHCFG_MODULE_CONF} ]]; then
		_shcfg__parse_module_cfg "${SHCFG_MODULE_CONF}"
		if [[ ${#_SHCFG_ACTIVE_MODULES[@]} == 0 ]]; then
			echo "[ERROR] No shcfg modules configured! Please specify some in '${SHCFG_MODULE_CONF}'" >&2
			return 1
		fi
	fi
	if [ "${SHCFG_DEBUG}" = 'true' ]; then
		echo "[DEBUG] _SHCFG_MODULE_ALIASES:"
		for alias in "${!_SHCFG_MODULE_ALIASES[@]}"; do
			echo "[DEBUG]  * [${alias}]=${_SHCFG_MODULE_ALIASES[${alias}]}" >&2
		done
		echo "[DEBUG] _SHCFG_ACTIVE_MODULES:" >&2
		for module in "${_SHCFG_ACTIVE_MODULES[@]}"; do
			echo "[DEBUG]  * ${module}" >&2
		done
	fi


	# ensure that activated modules are installed
	for module in "${_SHCFG_ACTIVE_MODULES[@]}"; do
		_shcfg__install_module "${module}"
	done


	# load activated modules
	for module in "${_SHCFG_ACTIVE_MODULES[@]}"; do
		module_file="${_SHCFG_MODULES_DIR}/${module}/module.bash"
		if [[ -f ${module_file} && -r ${module_file} ]]; then
			[ "${SHCFG_DEBUG}" = 'true' ] && echo "[INFO]  loading module: ${module}" >&2
			source "${module_file}"
		fi
	done


	# cleanup data dirs
	if [ "${SHCFG_CLEANUP_MODULE_REPOS:-true}" = 'true' ]; then
		_shcfg__cleanup_repos
	fi
	if [ "${SHCFG_CLEANUP_MODULES:-true}" = 'true' ]; then
		_shcfg__cleanup_modules
	fi
}

_shcfg__init "$@"
