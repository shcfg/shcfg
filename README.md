# SHCFG

SHCFG is another module-based shell configuration management framework which is
heavily influenced by [Bash-it](https://github.com/Bash-it/bash-it), but enriches
the concept with decentralized module repositories and declarative configuration.
This makes it a good (i'd say perfect, but i guess opinions might differ here) fit
for setups which include a dotfile management.

Take a look and decide for yourself whether you like the concept or not.

## License
SHCFG is licensed under the GNU General Public License version 3 or (at your option)
any later version of that license. See the [LICENSE.txt](./LICENSE.txt) file for
the full license text.

## Quick-Start
The framework provides you a `shcfg` command which you can use to control the behavior
of the framework and customize your experience.

To list all installable modules, type:  
`shcfg list --installable`

To install for example the module `greeter` from the default module registry, type:  
```shell
shcfg install 'default-repo/greeter'
```

All installed packages will be loaded on startup automatically. (This will change
once we get past the MVP stage)

To list all installed packages, type:  
```shell
shcfg list
```

If you want to uninstall/deactivate the greeter module again, you can type:  
```shell
shcfg uninstall 'default-repo/greeter'
```

## Installation
To install SHCFG you have to clone the git repository:  
```shell
git clone 'https://github.com/shcfg/shcfg.git' ~/.shcfg
```

The next step would be to include shcfg into your shell. To accomplish this, you
have to extend either your `~/.bashrc`, `~/.bash_profile` or `~/.profile`. (Use 
the first one you find or create a `~/.bashrc` file)  
Add following code snippet at the bottom of your rc file:
```shell
export SHCFG_HOME="${HOME}/.shcfg"
source "${SHCFG_HOME}/shcfg.bash"
```

The last thing you need to do is to start a new shell, so that the new configuration
will be active. In the new shell you should now have access to the commands described
below.  
To check your installation you can execute:
```shell
shcfg --version
```

## Updates
At the moment shcfg has no way to perform any automated updates, but a `shcfg update`
subcommand is planned.  
For now, you can update shcfg manually by leveraging the fact, that it is basically
a wrapper around Git. Therefore you can update shcfg, the module repositories and
the modules by executing simple `git pull` commands in the correct directories.
However, you should check whether any of the repositories or modules have any
submodules. If so, then you will have to update them as well by executing:
`git submodule update --init --recursive`.  
The directories in which you have to perform these actions are the following:
- `${SHCFG_HOME}` - the directory where shcfg is installed on your machine
- `${SHCFG_DATA_DIR}/repositories/*` - each child directory of the `repositories`
   data dir
- `${SHCFG_DATA_DIR}/modules/*` - each child directory of the `modules` data dir

For the default values of the environment variables take a look at the section
[Environment Variables](#environment-variables)

## Configuration
The goal for SHCFG is to be very flexible, so that as many people as possible are
enabled to configure it for their needs. Therefore it provides many configuration
options, both via configuration files as well as via environment variables.

### Config Files
SHCFG has two configuration files: `repositories.conf` and `modules.conf`. Usually
it should not be necessary to edit those files manually. One of the goals of SHCFG
is to enable the user to do all the configurations with the CLI.

#### repositories.conf
The `repositories.conf` file is located at `~/.config/shcfg/` per default (see
`SHCFG_CONF_DIR` at [Environment Variables](#environment-variables)) and contains
a list of known [module repositories](#module-repositories).

Three types of lines are allowed:

| Type           | Example                                            |
| -------------- | -------------------------------------------------- |
| Comment        | `# this is a comment`                              |
| Inclusion      | `!include repositories.d/*.conf`                   |
| Repo Reference | `@default https://github.com/shcfg/shcfg-repo.git` |

**Comments:**  
Comments are lines that are simply ignored and can, for example, be used to add
descriptions or to temporarily deactivate some repositories.

**Inclusions:**  
Inclusions enable the user to include other repo config files. This makes it easy
to set up dynamic but reusable environments with dotfile managers like
[Homesick](https://github.com/technicalpickles/homesick).  
The path can be absolute or relative to the config file and contain any patterns
which Bash accepts as globbing patterns.

**Repo References**  
This section only describes, how module repositories can be referenced in the config
file. If you want to understand how the repository system works, take a look at
the section [Module Repositories](#module-repositories).  
Repository references can be defined in a number of ways. The simplest form is just
a git-compatible URL. This will then be used to clone the Git-repository of the
module repository. This also means you can enter any URL or path here, that Git
would also accept for a `git clone`.  
The repository name would then be extracted from the URL/path by taking the last
path section and stripping-off any trailing slashes and '*.git*' suffixes. The
repository name can be customized by prepending an alias-section.  
An alias-section consists of a leading '*@*' sign, followed by an arbitrary length
name specifier which is delimitted by a space character. For example, in the table
above the name of the repository would be '*default*'. The alias-section and the
repository reference are separated by one or more whitespace (space or tab) characters.

#### modules.conf
The `modules.conf` file is located at `~/.config/shcfg/` per default (see `SHCFG_CONF_DIR`
at [Environment Variables](#environment-variables)) and contains the list of activated
[modules](#modules). As you will see, the `modules.conf` file has some similarities
with the `repositories.conf` file from the last section.

Three types of lines are allowed:

| Type             | Example                     |
| ---------------- | --------------------------- |
| Comment          | `# this is a comment`       |
| Inclusion        | `!include modules.d/*.conf` |
| Module Reference | `@bar foo-repo/bar-module`  |

**Comments:**  
Comments are lines that are simply ignored and can, for example, be used to add
descriptions or to temporarily deactivate some modules.

**Inclusions:**  
Inclusions enable the user to include other module config files. This makes it easy
to set up dynamic but reusable environments with dotfile managers like
[Homesick](https://github.com/technicalpickles/homesick).  
The path can be absolute or relative to the config file and contain any patterns
which Bash accepts as globbing patterns.

**Module References:**  
This section only describes, how modules can be activated by altering the configuration
file. If you want to understand how the module system works, take a look at the
[modules section](#modules).  
Module references describe the modules which are activated and therefore loaded
on startup. The simplest module reference consists of a module repository name followed
by a module name. Repository name and module name are separated by a slash.  
If the module name is unambiguous across all known module repositories, the repository
name is optional. So if there exists only one `bar-module`, that reference would
be specific enough.  
You can define an alias for the module via a prepended alias section. Such an alias
can for example be used for the [uninstall](#shcfg-uninstall) subcommand (more
usages will follow).  
An alias-section consists of a leading '*@*' sign, followed by an arbitrary
length name specifier which is delimitted by a space character. For example, in
the table above the name of the module would be '*bar*'. The alias-section and the
module reference are separated by one or more whitespace (space or tab) characters.

### Environment Variables
The behavior of SHCFG can be customized by editing following environment variables:

| Variable Name     | Default Value                         | Description                                                                                                                                                                                |
| ----------------- | ------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| SHCFG_CONF_DIR    | `${XDG_CONFIG_HOME}/shcfg`            | Directory which contains the configuration files                                                                                                                                           |
| SHCFG_DATA_DIR    | `${XDG_DATA_HOME}/shcfg`              | Directory which contains the application data.<br> (The content in this directory will be generated according to the configuration files)                                                  |
| SHCFG_DEBUG       | *Not set by default*                  | If set to `true`, enables debug output                                                                                                                                                     |
| SHCFG_HOME        | *Determined dynamically*              | The base directory of your SHCFG installation.<br> (If this variable is unset, SHCFG tries to determine the value dynamically by evaluating `${BASH_SOURCE}`)                              |
| SHCFG_MODULE_CONF | `${SHCFG_CONF_DIR}/modules.conf`      | The location of the modules.conf file (see [modules.conf](#modulesconf))                                                                                                                  |
| SHCFG_REPO_CONF   | `${SHCFG_CONF_DIR}/repositories.conf` | The location of the repositories.conf file (see [repositories.conf](#repositoriesconf))                                                                                                   |
| XDG_CONFIG_HOME   | `${HOME}/.config`                     | The directory for user-specific configuration files<br>(according to the [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)) |
| XDG_DATA_HOME     | `${HOME}/.local/share`                | The directory for user-specific data files<br>(according to the [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html))          |

## Commands
SHCFG provides the `shcfg` command to assist in managing the settings. It has the
subcommands `help`, `list`, `install` and `uninstall` which are described in the
following sections.

### shcfg help
**Usage:** `shcfg help [TOPIC]`

The help command shows usage information and serves as live-documentation for the
different subcommands. It shows some common usage information like the available
subcommands and other help sections if called without any further options.  
To get more detailed information about a specific subcommand, the name of that
subcommand can be provided as additional argument.

**Example:** `shcfg help list`  
This would show a detailed usage message for the `list` subcommand.

### shcfg reload
**Usage:** `shcfg reload`

Re-initialize shcfg. This is for example useful if you did edit the config files
manually and want shcfg to recognize the changes.

### shcfg list
**Usage:** `shcfg list [MODE] [OPTIONS...]`

The list command can show a table of all known modules or module repositories according
to the applied filters. It's for example possible to show only the installed or
only the available modules.  
Please note, that module names are currently always fully qualified (repo-name/module-name).

**Available Modes:**

| Mode    | Description                              |
| ------- | ---------------------------------------- |
| modules | list modules and theme modules (default) |
| repos   | list module repositories                 |

**Available Options:**  
Any number of the following options can be used to modify the resulting list.
However, please note that the two modes don't accept the same set of options.

**Available Module Status Filters: (module-mode only)**

| Short | Long           | Description                          |
| ----- | -------------- | ------------------------------------ |
| -a    | --available    | show currently not installed modules |
| +a    | --no-available | hide currently not installed modules |
| -i    | --installed    | show currently installed modules     |
| +i    | --no-installed | hide currently installed modules     |
|       | --all          | combines --installed and --available |

**Available Module List Options: (module-mode only)**

| Short | Long        | Description        |
| ----- | ----------- | ------------------ |
| -S    | --status    | show status column |
| +S    | --no-status | hide status column |

**Default values:**  
If not specified otherwise the following defaults are active:
* mode: `modules`
* status filters: `--installed`
* module list options:
    * `--status` (if more than one status filter is active)
    * `--no-status` (if only one status filter is active)

Therefore the default action for `shcfg list` is equivalent to:
```shell
shcfg list modules --installed --no-status
```

**Example:** `shcfg list --all`  
This shows a table of all known modules. Each entry contains the module name,
the active alias for that module and the current state of that module. If no alias
is active for the module, the alias column is set to: `-`.

### shcfg install
**Usage:** `shcfg install MODULE [MODULE...]`  
Install one ore more modules. A **MODULE** specifier is resolved by first checking
whether it is a known module-alias, or else if it is a module-name reference in
the form: `[<repo-name>/]<module-name>`. The repo name is only necessary if the
module name is ambiguous.

**Exit codes:**

| Exit Code | Meaning                                                             |
| --------- | ------------------------------------------------------------------- |
| 0         | Everything ok, all modules successfully installed                   |
| 1         | Some modules successfully installed, some modules failed to install |
| 2         | All modules failed to install                                       |

### shcfg uninstall
**Usage:** `shcfg uninstall MODULE [MODULE...]`  
Uninstall one ore more modules. A **MODULE** specifier is resolved by first checking
whether it is a known module alias, or else if it is a module-name reference in
the form: `[<repo-name>/]<module-name>`. The repo name is only necessary if the
module name is ambiguous. SHCFG ensures that the modules are deactivated (removed
from the config file) after uninstalling them.

**Exit codes:**

| Exit Code | Meaning                                                                 |
| --------- | ----------------------------------------------------------------------- |
| 0         | Everything ok, all modules successfully uninstalled                     |
| 1         | Some modules successfully uninstalled, some modules failed to uninstall |
| 2         | All modules failed to uninstall                                         |

## Understanding the Extension Mechanism
SHCFG aims to be highly customizable by offering a git-repo based extension mechanism.

### Module Repositories
Module repositories serve as an index of modules available for install. They are
basically git-repos which contain a `modules.list` file similar to the
global config files ([repositories.conf](#repositoriesconf) and
[modules.conf](#modulesconf)).

Two types of lines are allowed in the `modules.list` file:

| Type             | Example                                                |
| ---------------- | ------------------------------------------------------ |
| Comment          | `# this is a comment`                                  |
| Module Reference | `@default https://github.com/shcfg/example-module.git` |

**Comments:**  
Comments are lines that are simply ignored and can, for example, be used to add
descriptions or to temporarily deactivate some repositories.

**Module References**  
Module references can be defined in a number of ways. The simplest form is just
a git-compatible URL. This will then be used to clone the Git-repository of the
module. This also means you can enter any URL or path here, that Git
would also accept for a `git clone`.  
The module name would then be extracted from the URL/path by taking the last path
section and stripping-off any trailing slashes and '*.git*' suffixes. The module
name can be customized by prepending an alias-section.  
An alias-section consists of a leading '*@*' sign, followed by an arbitrary length
name specifier which is delimitted by a space character. For example, in the table
above the name of the module would be '*default*'. The alias-section and the repository
reference are separated by exactly one space char.

On startup the module repositories will be cloned (`git clone ...`) accordingly
if they are not already installed. If the repository contains any git submodules,
they will be initialized recursively. For example this makes it possible to include
modules as git submodules and to reference them with a relative filesystem path
in the `modules.list` file.  
The module repositories will be installed to `${SHCFG_DATA_DIR}/repositories/<REPO_NAME>`
per default. (see [Environment Variables](#environment-variables))

### Modules
Modules are the core of the SHCFG system. They are defined by [Module Repositories](#module-repositories)
and enable you to extend your shell in various ways.  
Some examples are:
* defining functions
* defining environment variables
* performing some checks and presenting the report
* executing any other tasks you may come up with on shell initialization

Modules are basically git-repos (just like module repositories) that contain at
least a `module.bash` file.  
On startup, SHCFG will ensure that all activated (see [modules.conf](#modulesconf))
modules are installed via `git clone`. Any git submodules will be initialized correctly.
Afterwards the `module.bash` file of every installed module will be sourced into
the active bash session.

## Startup Process
This section describes the actions shcfg performs when starting up.

 1. If the environment variable `SHCFG_HOME` is not set, shcfg tries to guess it
    by evaluating `BASH_SOURCE`.
 2. Loading the different libraries of shcfg. This step also defines some environment
    variables. (see [/lib/env.sh](/lib/env.sh))
 3. Load the repository list according to the repository configuration file.  
    The repository configuration file is the first readable file determined by following
    queries: (see [Environment Variables](#environment-variables) for default values)
    1. `${SHCFG_REPO_CONF}`
    2. `${SHCFG_CONF_DIR}/repositories.conf`
 4. Ensure that all module repositories are installed and correctly set up.
 5. Build a module index by reading the `modules.list` file of each module repository.
 6. Load the list of activated modules according to the module configuration file.  
    The module configuration file is the first readable file determined by following
    queries: (see [Environment Variables](#environment-variables) for default values)
    1. `${SHCFG_MODULE_CONF}`
    2. `${SHCFG_CONF_DIR}/modules.conf`
 7. Ensure that all activated modules are installed and correctly set up.
 8. Load the activated modules by sourcing them into the current shell session.
 9. Cleanup module repositories by removing any locally installed module repositories
    which are not configured in the module repositories config. (see 3.)  
    This step can be skipped by setting `SHCFG_CLEANUP_MODULE_REPOS` to `false`.
10. Cleanup modules by removing any locally installed modules which are not configured
    in the modules configuration. (see 6.)  
    This step can be skipped by setting `SHCFG_CLEANUP_MODULES` to `false`.
