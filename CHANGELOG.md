# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
This section describes the changes that are currently being worked on and will
probably be released in the next version.
### Add
- Easy updates (framework, repo and module) with the `shcfg update` subcommand
- Status checks with the `shcfg status` subcommand
- Automated tests using [Bats](https://github.com/bats-core/bats-core/)
- Install/Uninstall scripts

## [0.1.0] - 2020-05-28
Initial release, implementation of a MVP.
### Added
- Command `shcfg` as command-line management interface
- Subcommand `shcfg help` to show an interactive usage guide
- Subcommand `shcfg list` to list the known modules and module repositories
- Subcommand `shcfg install` to install a module
- Subcommand `shcfg uninstall` to uninstall a module
- Subcommand `shcfg reload` to re-initialize the framework
- Options `shcfg -h` and `shcfg --help` as aliases for `shcfg help`
- Option `shcfg --license` to show the license text
- Option `shcfg --version` to show the version number
- Support to configure module repositories
- Support to configure installed/active modules
- Documentation in README.md
